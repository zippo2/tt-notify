package com.tt.notify.consumer.listener;

import java.util.List;

import com.tt.notify.common.constant.MQ;
import com.tt.notify.consumer.vo.MessageVO;

/**
 * 消费者监听接口，业务需要实现此接口并配置到Consumer中。
 * @author  liuhaihui
 * @date    2017年7月13日 下午7:20:50
 * @version
 */
public interface MQMessageListener {
	
	/**
	 * 获取本消费者的消费模式,取值为BOARDCAST/POINT-TO-POINT
	 * @return
	 */
	MQ.ConsumeType getMQConsumeType();
	
	/**
	 * 获取本消费者要监听的主题（topic）
	 * @return
	 */
	MQ.Topic getTopic();
	
	/**
	 * 获取本消费者要监听的主题下面的一个到多个标签（tag）
	 * @return
	 */
	List<MQ.Tag> getTagList();
	
	/**
	 * 监听消息发送
	 * @param messages
	 * @param Context
	 * @return 消息处理成功请返回true,如果处理失败请返回false
	 */
	boolean onMessage(MessageVO messageVO);
}
