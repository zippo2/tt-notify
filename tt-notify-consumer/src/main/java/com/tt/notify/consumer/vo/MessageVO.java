/**
 *  创建日期: 2017年7月20日下午10:28:44
 *  @copyright (c) 2017, 82% All Rights Reserved. 
 */
package com.tt.notify.consumer.vo;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

/**
 * 消息实体
 * @author   82%
 * @date     2017年7月20日 下午10:28:44
 * @version  1.0 
 */
@Getter
@Setter
public class MessageVO implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 消息主题
	 */
	private String topic;
	
	/**
	 * 消息tags
	 */
	private String tags;
	
	/**
	 * 消息Keys
	 */
	private String keys;
	
	/**
	 * 消息内容
	 */
	private String content;

	/**
	 * 队列ID
	 */
	private int queueId;
	
	/**
	 * 队列偏移
	 */
	private long queueOffset;
	
	/**
	 * 消息ID
	 */
	private String msgId;
	
	/**
	 * 消息创建时间
	 */
	private long bornTimestamp;
	
	/**
	 * 消息回队列的次数
	 */
	private int reconsumeTimes;
}

