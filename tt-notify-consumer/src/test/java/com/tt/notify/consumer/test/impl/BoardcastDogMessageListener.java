/**
 *  创建日期: 2017年7月14日上午11:09:10
 *  @copyright (c) 2017, 82% All Rights Reserved. 
 */
package com.tt.notify.consumer.test.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.tt.notify.common.constant.MQ;
import com.tt.notify.common.constant.MQ.ConsumeType;
import com.tt.notify.common.constant.MQ.Tag;
import com.tt.notify.common.constant.MQ.Topic;
import com.tt.notify.consumer.listener.MQMessageListener;
import com.tt.notify.consumer.vo.MessageVO;

/**
 * @author   82%
 * @date     2017年7月14日 上午11:09:10
 * @version  1.0 
 */
@Component
public class BoardcastDogMessageListener implements MQMessageListener {

	@Override
	public ConsumeType getMQConsumeType() {
		return MQ.ConsumeType.BOARDCAST;
	}
	
	/**
	 * 类似于 topic=PAY
	 * @return
	 */
	@Override
	public Topic getTopic() {
		return MQ.Topic.TOPIC_DOG;
	}

	/**
	 * 类似于  tag=PAY_SUCCESS || PAY_FAIL
	 * @return
	 */
	@Override
	public List<Tag> getTagList() {
		List<Tag> tagList = new ArrayList<Tag>();
		tagList.add(MQ.Tag.TAG_DOG_WANGWANG);
		tagList.add(MQ.Tag.TAG_DOG_XIAOHEI);
		tagList.add(MQ.Tag.TAG_DOG_BAIMAO);
		//tagList.add(MQ.Producer.Tag.ALL);
		return tagList;
	}

	/**
	 * 处理接收到的消息
	 * @param messages
	 * @param Context
	 * @return 消息处理成功请返回true,如果处理失败请返回false
	 */
	@Override
	public boolean onMessage(MessageVO messageVO) {
		/**
		 * 消息处理成功请返回true,如果处理失败请返回false
		 */
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String now = sdf.format(new java.util.Date());
		
        /*
         * 执行Topic的消费逻辑
         */
		if(MQ.Tag.TAG_DOG_WANGWANG.getName().equals(messageVO.getTags())) {
			System.err.println("[ "+ now + " ]收到了一条消息|content=" + messageVO.getContent() 
								+ ", topic="+ messageVO.getTopic() 
								+ ", tags=" + messageVO.getTags()
								+ ", 这条狗叫[旺旺]"
								);
        	return true;
    	}
    	else if(MQ.Tag.TAG_DOG_XIAOHEI.getName().equals(messageVO.getTags())) {
    		System.err.println("[ "+ now + " ]收到了一条消息|content=" + messageVO.getContent() 
								+ ", topic="+ messageVO.getTopic() 
								+ ", tags=" + messageVO.getTags()
								+ ", 这条狗叫[小黑]"
								);
    		return true;
    	}
    	else if(MQ.Tag.TAG_DOG_BAIMAO.getName().equals(messageVO.getTags())) {
    		System.err.println("[ "+ now + " ]收到了一条消息|content=" + messageVO.getContent() 
								+ ", topic="+ messageVO.getTopic() 
								+ ", tags=" + messageVO.getTags()
								+ ", 这条狗叫[白毛]"
								);
    		return true;
    	}
		System.err.println("这条狗我不认识");
		return false;
	}

}

