/**
 *  创建日期: 2017年7月13日下午5:59:49
 *  @copyright (c) 2017, 82% All Rights Reserved. 
 */
package com.tt.notify.consumer.test;

import java.text.SimpleDateFormat;
import java.util.Scanner;

import org.apache.log4j.PropertyConfigurator;
import org.springframework.context.support.ClassPathXmlApplicationContext;


/**
 * @author   82%
 * @date     2017年7月13日 下午5:59:49
 * @version  1.0 
 */
public class MQ_PointToPoint_Bird_ConsumerMain {

	@SuppressWarnings("resource")
	public static void main(String[] args) {
		
		PropertyConfigurator.configure(MQ_PointToPoint_Bird_ConsumerMain.class.getClassLoader().getResource("log4j.properties")); 
		
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(new String[]{
				"classpath*:conf/spring/spring-context-*.xml",
		});
		context.start();


		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String now = sdf.format(new java.util.Date());
		System.out.println("\n\n[点对点消费] MQ消费者已经启动！[" + now + "]\n\n      |- 请输入任意字符结束:");

		for(;;){
			Scanner scanner = new Scanner(System.in);
			String str = scanner.nextLine();
			if("exit".equals(str)){
				break;
			}
		}
		
		context.registerShutdownHook();
		context.destroy();
		context.close();
	}
}

