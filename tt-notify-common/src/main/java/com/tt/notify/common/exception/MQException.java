/**
 *  创建日期: 2017年7月13日下午7:25:34
 *  @copyright (c) 2017, 82% All Rights Reserved. 
 */
package com.tt.notify.common.exception;

/**
 * 消息异常类
 * @author   82%
 * @date     2017年7月13日 下午7:25:34
 * @version  1.0 
 */
public class MQException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public MQException() {
        super();
    }

    public MQException(String message) {
        super(message);
    }

    public MQException(String message, Throwable cause) {
        super(message, cause);
    }
    
    public MQException(Throwable cause) {
        super(cause);
    }
    
}

