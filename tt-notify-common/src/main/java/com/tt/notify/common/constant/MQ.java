/**
 *  创建日期: 2017年7月13日下午8:56:41
 *  @copyright (c) 2017, 82% All Rights Reserved. 
 */
package com.tt.notify.common.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author   82%
 * @date     2017年7月13日 下午8:56:41
 * @version  1.0 
 */
public interface MQ {

	static final String GROUP_NAME_PRIFIX = "MQ-PRODUCER-CONSUMER-GROUP-NAME-";
	
	public enum ConsumeType {
		BOARDCAST,		//广播模式
		POINTTOPOINT;	//点对点消费模式
	}
	

	@Getter
	@AllArgsConstructor
	public enum Topic {

		ALL("*", "所有主题的消息", GROUP_NAME_PRIFIX + "ALL"),
		
		PAY("PAY", "支付", GROUP_NAME_PRIFIX + "PAY"),
		ORDER("ORDER", "订单", GROUP_NAME_PRIFIX + "ORDER"),
		USER("USER", "用户", GROUP_NAME_PRIFIX + "USER"),
		
		/**
		 * 下面的是测试topic
		 */
		TOPIC_CAT("TOPIC_CAT", "猫", GROUP_NAME_PRIFIX + "CAT"),
		TOPIC_DOG("TOPIC_DOG", "狗", GROUP_NAME_PRIFIX + "DOG"),
		TOPIC_PIG("TOPIC_PIG", "猪", GROUP_NAME_PRIFIX + "PIG"),
		TOPIC_BIRD("TOPIC_BIRD", "鸟", GROUP_NAME_PRIFIX + "BIRD"),
		TOPIC_DUCK("TOPIC_DUCK", "鸭子", GROUP_NAME_PRIFIX + "DUCK"),
		
		;
		
		private String name;
		private String desc;
		/**
		 * 同一个topic必须由同一个生产者组合消费组组名去处理。
		 * 		|- 否则会出现消息长时间delay，消息消费不到等乱七八糟的问题。
		 * 对RMQ的机制尚不清楚的情况下暂时这么做
		 */
		private String producerAndConsumerGroupName;

		
	}

	@Getter
	@AllArgsConstructor
	public enum Tag {
		
		ALL("*", "所有标签的消息"),
		
		PAY_SUCCESS("PAY_SUCCESS", "支付成功"),
		PAY_FAIL("PAY_FAIL", "支付失败"),
		
		ORDER_CHARGE("ORDER_CHARGE", "365订单"),
		ORDER_INSTALLMENT("ORDER_INSTALLMENT", "超额支付订单"),
		
		USER_CONTRACTS("USER_CONTRACTS", "用户人脉"),

		ORDER_QUERY("ORDER_QUERY", "订单查询"),
		PROXY_QUERY("PROXY_QUERY", "代理查询"),
		
		/**
		 * 下面的是测试tag
		 */
		TAG_CAT("TAG_CAT", "猫"),
		
		TAG_DOG_WANGWANG("TAG_DOG_WANGWANG", "旺旺"),
		TAG_DOG_XIAOHEI("TAG_DOG_XIAOHEI", "小黑"),
		TAG_DOG_BAIMAO("TAG_DOG_BAIMAO", "白毛"),
		
		TAG_PIG("TAG_PIG", "猪"),
		
		TAG_BIRD_HAWK("TAG_BIRD_HAWK", "隼"),
		TAG_BIRD_HERON("TAG_BIRD_HERON", "苍鹰"),
		TAG_BIRD_VULTURE("TAG_BIRD_VULTURE", "秃鹫"),
		TAG_BIRD_BALD_EAGLE("TAG_BIRD_BALD_EAGLE", "白头鹰"),
		
		TAG_DUCK("TAG_DUCK", "鸭子"),
		
		;
		
		private String name;
		private String desc;
		
	}
}

