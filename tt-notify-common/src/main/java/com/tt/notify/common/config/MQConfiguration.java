package com.tt.notify.common.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;

/**
 * 消息-全局配置文件
 * @author  liuhaihui
 * @date    2017年7月13日 下午7:31:21
 * @version
 */
@Getter
@Setter
@Component
public class MQConfiguration {

	/**
	 * 全局配置
	 * 不需要设置instanceName
	 */
	@Value("${namesrvAddr}")
	private String namesrvAddr;
}
