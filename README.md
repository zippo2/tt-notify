
本项目是一个组件，是对RocketMQ3.2.6版本的封装，支持广播模式和点对点模式。如果升级RocketMQ到最新版4.0.1-incubating也可以使用本工程，只需要将com.alibaba.rocketmq报名修改为com.apache.rocketmq即可。

本项目地址为：
https://gitee.com/zippo2/tt-notify.git


如使用过程中有任何疑问或bug，可以联系我。

邮件或QQ联系方式：3275632084@qq.com(昵称82%)